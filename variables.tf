variable gitlab_runner_terraform_token {
  type        = string
  description = "Gitlab runner registration token for the terraform gitlab runner"
}
variable google_provider_client_id {
  type        = string
  description = "Client id for Google cognito provider"
}
variable google_provider_client_secret {
  type        = string
  description = "Client secret for Google cognito provider"
}
