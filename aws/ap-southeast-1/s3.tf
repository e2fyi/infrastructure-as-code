
module "s3-logs" {
  source = "git::https://gitlab.com/e2fyi/terraform-aws-eks.git//modules/s3"

  bucket        = "e2fyi-logs"
  acl           = "log-delivery-write"
  versioning    = false
  sse_algorithm = "AES256"

  folder_to_expiration_days = {
    log = 3
  }
  tags = local.tags
}

module "s3-terraform" {
  source = "git::https://gitlab.com/e2fyi/terraform-aws-eks.git//modules/s3"

  bucket        = "e2fyi-vault"
  versioning    = false
  sse_algorithm = "AES256"

  folder_to_expiration_days = {
    tmp = 7
  }
  logging = {
    "${module.s3-logs.id}" = "log/s3/e2fyi-vault"
  }

  tags = local.tags
}

module "s3-athena" {
  source = "git::https://gitlab.com/e2fyi/terraform-aws-eks.git//modules/s3"

  bucket        = "e2fyi-athena"
  versioning    = false
  sse_algorithm = "AES256"

  folder_to_expiration_days = {
    tmp = 7
  }
  logging = {
    "${module.s3-logs.id}" = "log/s3/e2fyi-athena"
  }

  tags = local.tags
}
